#include <stdio.h>
#include <stdlib.h>

#include "esp_log.h"
#if defined(CONFIG_WALK_TYPE_SERVER)
#include "gatt_server.h"
#elif defined(CONFIG_WALK_TYPE_CLIENT)
#include "gatt_client.h"
#endif
#include "motors.h"

void
app_main(void)
{
	ledc_init();
#if defined(CONFIG_WALK_TYPE_SERVER)
	initialize_ble_server();
#elif defined(CONFIG_WALK_TYPE_CLIENT)
	initialize_ble_client();
	test_pair_vib();
#endif
}
