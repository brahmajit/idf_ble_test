#ifndef BLE_GATT_SERVER_H
#define BLE_GATT_SERVER_H
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Attributes State Machine */
enum {
	IDX_SVC,
	IDX_CHAR_A,
	IDX_CHAR_VAL_A,
	IDX_CHAR_CFG_A,

	HRS_IDX_NB,
};

void initialize_ble_server(void);
#endif // !BLE_GATT_SERVER_H
