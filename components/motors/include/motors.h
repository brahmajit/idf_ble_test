#ifndef __MOTOR_H__
#define __MOTOR_H__
#include "esp_err.h"

esp_err_t ledc_init(void);
void short_beep(int);
void long_beep(int);
void beep(int);
void led_update(int);
#endif // !__MOTOR_H__
