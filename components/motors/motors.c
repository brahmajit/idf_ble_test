#include <stdio.h>
#include <math.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/portmacro.h"
#include "esp_err.h"
#include "esp_log.h"
#include "hal/ledc_types.h"
#include "driver/ledc.h"
#include "motors.h"

#define LEDC_TIMER LEDC_TIMER_0
#define LEDC_MODE LEDC_LOW_SPEED_MODE
#define LEDC_OUTPUT_IO_0 CONFIG_WALK_MOTOR_IO_1 // Define the output GPIO
#define LEDC_OUTPUT_IO_1 CONFIG_WALK_MOTOR_IO_2 // Define the output GPIO
#define LEDC_DUTY_RES LEDC_TIMER_2_BIT          // Set duty resolution to 2 bits
#define LEDC_FREQUENCY (20000)                  // Frequency in Hertz. Set frequency at 20 kHz

static int ret;

esp_err_t
ledc_init(void)
{
	/*
	 * Prepare and set configuration of timers
	 * that will be used by LED Controller
	 */
	ledc_timer_config_t ledc_timer = {
	    .duty_resolution = LEDC_DUTY_RES, // resolution of PWM duty
	    .freq_hz = LEDC_FREQUENCY,        // frequency of PWM signal
	    .speed_mode = LEDC_MODE,          // timer mode
	    .timer_num = LEDC_TIMER,          // timer index
	    .clk_cfg = LEDC_AUTO_CLK,         // Auto select the source clock
	};

	ESP_ERROR_CHECK(ledc_timer_config(&ledc_timer));

	/*
	 * Prepare individual configuration
	 * for each channel of LED Controller
	 * by selecting:
	 * - controller's channel number
	 * - output duty cycle, set initially to 0
	 * - GPIO number where LED is connected to
	 * - speed mode, either high or low
	 * - timer servicing selected channel
	 *   Note: if different channels use one timer,
	 *         then frequency and bit_num of these channels
	 *         will be the same
	 */
	ledc_channel_config_t ledc_channel[2] = {
	    {.channel = LEDC_CHANNEL_0,
	     .duty = 0,
	     .gpio_num = LEDC_OUTPUT_IO_0,
	     .speed_mode = LEDC_MODE,
	     .hpoint = 0,
	     .timer_sel = LEDC_TIMER,
	     .flags.output_invert = 0},
	    {.channel = LEDC_CHANNEL_1,
	     .duty = 0,
	     .gpio_num = LEDC_OUTPUT_IO_1,
	     .speed_mode = LEDC_MODE,
	     .hpoint = 0,
	     .timer_sel = LEDC_TIMER,
	     .flags.output_invert = 0},
	};

	// Set LED Controller with previously prepared configuration
	for (int i = 0; i < 2; ++i) {
		ESP_ERROR_CHECK(ledc_channel_config(&ledc_channel[i]));
	}

	return ESP_OK;
}

void
led_update(int vib_strength)
{
	ret = ledc_set_duty(LEDC_MODE, LEDC_CHANNEL_0, vib_strength);
	if (ret != ESP_OK)
		return;
	ret = ledc_set_duty(LEDC_MODE, LEDC_CHANNEL_1, vib_strength);
	if (ret != ESP_OK)
		return;
	ret = ledc_update_duty(LEDC_MODE, LEDC_CHANNEL_0);
	if (ret != ESP_OK)
		return;
	ret = ledc_update_duty(LEDC_MODE, LEDC_CHANNEL_1);
	if (ret != ESP_OK)
		return;
}
